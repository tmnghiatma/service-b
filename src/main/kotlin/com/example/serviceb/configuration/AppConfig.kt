package com.example.serviceb.configuration

import org.elasticsearch.client.RequestOptions
import org.elasticsearch.client.RestHighLevelClient
import org.elasticsearch.client.indices.CreateIndexRequest
import org.elasticsearch.client.indices.GetIndexRequest
import org.slf4j.LoggerFactory
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.context.annotation.Configuration
import javax.annotation.PostConstruct

@Configuration
class AppConfig(@Autowired var elasticsearchClient: RestHighLevelClient) {
    private val logger = LoggerFactory.getLogger(javaClass)

    @PostConstruct
    fun createIndex() {
        val request = GetIndexRequest("daily_covid_info")
        val isExist = elasticsearchClient.indices().exists(request, RequestOptions.DEFAULT)
        if (!isExist) {
            logger.info("Creating the index daily_covid_info")
            val createIndexRequest = CreateIndexRequest("daily_covid_info")
            elasticsearchClient.indices().create(createIndexRequest, RequestOptions.DEFAULT)
        }
    }
}