package com.example.serviceb.configuration

import com.example.serviceb.model.CovidInfo
import org.apache.kafka.clients.consumer.ConsumerConfig
import org.apache.kafka.common.serialization.StringDeserializer
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.core.env.Environment
import org.springframework.kafka.annotation.EnableKafka
import org.springframework.kafka.config.ConcurrentKafkaListenerContainerFactory
import org.springframework.kafka.config.KafkaListenerContainerFactory
import org.springframework.kafka.core.ConsumerFactory
import org.springframework.kafka.core.DefaultKafkaConsumerFactory
import org.springframework.kafka.listener.ConcurrentMessageListenerContainer

@EnableKafka
@Configuration
class KafkaConsumerConfig(@Autowired val env: Environment) {

    private fun consumerConfigs(): Map<String, Any> {
        val host = env.getProperty("kafka.consumer.host", "localhost")
        val port = env.getProperty("kafka.consumer.port", "9092")
        val consumerGroup = env.getProperty("kafka.consumer.group", "consumer-group")
        val props = HashMap<String, Any>()

        props[ConsumerConfig.BOOTSTRAP_SERVERS_CONFIG] = "$host:$port"
        props[ConsumerConfig.KEY_DESERIALIZER_CLASS_CONFIG] = StringDeserializer::class.java
        props[ConsumerConfig.VALUE_DESERIALIZER_CLASS_CONFIG] = StringDeserializer::class.java
        props[ConsumerConfig.GROUP_ID_CONFIG] = consumerGroup

        return props
    }

    @Bean
    fun consumerFactory(): ConsumerFactory<String, CovidInfo> {
        return DefaultKafkaConsumerFactory(consumerConfigs())
    }

    @Bean
    fun kafkaListenerContainerFactory(): KafkaListenerContainerFactory<ConcurrentMessageListenerContainer<String, CovidInfo>> {
        val factory = ConcurrentKafkaListenerContainerFactory<String, CovidInfo>()
        factory.consumerFactory = consumerFactory()

        return factory
    }
}