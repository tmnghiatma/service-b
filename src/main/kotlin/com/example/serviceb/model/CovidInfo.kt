package com.example.serviceb.model

import com.google.gson.Gson
import java.time.LocalDate

data class CovidInfo(
    val country: String,
    val cases: Double,
    val deaths: Double,
    val recovered: Double,
    val date: String = LocalDate.now().toString()
) {
    override fun toString(): String {
        return Gson().toJson(this)
    }
}
