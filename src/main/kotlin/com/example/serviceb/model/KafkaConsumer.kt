package com.example.serviceb.model

import org.elasticsearch.action.index.IndexRequest
import org.elasticsearch.client.RequestOptions
import org.elasticsearch.client.RestHighLevelClient
import org.elasticsearch.common.xcontent.XContentType
import org.slf4j.LoggerFactory
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.kafka.annotation.KafkaListener
import org.springframework.stereotype.Component

@Component
class KafkaConsumer(@Autowired var elasticsearchClient: RestHighLevelClient) {
    private val logger = LoggerFactory.getLogger(javaClass)

    @KafkaListener(topics = ["daily-covid-info"])
    fun receive(payload: String) {
        logger.info("Received payload: {}", payload)

        val request = IndexRequest("daily_covid_info")
        elasticsearchClient.index(request.source(payload, XContentType.JSON), RequestOptions.DEFAULT)
    }
}